const axios = require('axios');

const {
  GraphQLObjectType,
  GraphQLInputObjectType,
  GraphQLInt,
  GraphQLString,
  GraphQLList,
  GraphQLSchema,
  GraphQLNonNull
} = require('graphql');

// Program
const Program = new GraphQLObjectType({
  name: 'Program',
  fields: () => ({
    mission_name: { type: GraphQLString },
    flight_number: { type: GraphQLInt },
    id:            { type: GraphQLInt },
    operation:     { type: GraphQLString },
    processId:     { type: GraphQLString },
    processStep:   { type: GraphQLInt },
    productFamily: { type: GraphQLString },
  })
});

const ProgramInputType = new GraphQLInputObjectType({
  name: 'ProgramInput',
  fields: () => ({
    id:            { type: GraphQLInt },
    operation:     { type: new GraphQLNonNull(GraphQLString) },
    processId:     { type: new GraphQLNonNull(GraphQLString) },
    processStep:   { type: new GraphQLNonNull(GraphQLInt) },
    productFamily: { type: new GraphQLNonNull(GraphQLString) },
  })
});

// Root Query
const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    programs: {
      type: new GraphQLList(Program),
      resolve(parent, args) {
        return axios
          .get('https://api.spacexdata.com/v3/launches')
          .then(res => res.data);
      }
    },
    program: {
      type: Program,
      args: {
        id: { type: GraphQLInt }
      },
      resolve(parent, args) {
        return axios
          .get(`https://api.spacexdata.com/v3/launches/${args.id}`)
          .then(res => res.data);
      }
    }
  }
});


const MutationType = new GraphQLObjectType({
  name: 'mutationProgram',
  description: 'Allows Add/Edit/Delete operations on Programs',
  fields: {
    createProgram: {
      type: Program,
      args: {
        operation:     { type: new GraphQLNonNull(GraphQLString) },
        processId:     { type: new GraphQLNonNull(GraphQLString) },
        processStep:   { type: new GraphQLNonNull(GraphQLInt) },
        productFamily: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve: (parent, args) => {
        return axios
          .post('https://jsonplaceholder.typicode.com/posts', { args })
          .then(res => res.data);
      }
    },

    updateProgram: {
      type: Program,
      args: {
        id:            { type: GraphQLInt },
        operation:     { type: new GraphQLNonNull(GraphQLString) },
        processId:     { type: new GraphQLNonNull(GraphQLString) },
        processStep:   { type: new GraphQLNonNull(GraphQLInt) },
        productFamily: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve: (parent, args) => {
        return axios
          .put(`https://jsonplaceholder.typicode.com/posts/${args.id}`, args)
          .then(res => res.data).
          catch((e)=> {
            console.log(e);
          });
      }
    },

    deleteProgram: {
      type: Program,
      args: {
        id: { type: GraphQLInt },
      },
      resolve: (parent, { id }) => {
        console.log(id)
        return axios
          .delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
          .then(res => console.logres.data);
      }
    },

  },
});

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: MutationType
});
